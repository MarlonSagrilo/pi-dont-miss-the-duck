/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Diego Lacerda
 */
public class clsGameNew {
    //private String Arma01;
    private clsUsuario Usuario;
    private int ArmaAtual;
    private clsArma Arma01;
    private clsArma Arma02;
    private int Nivel;
    private int PontosObtidos;
    private Boolean PassouNivel;

    public Boolean getPassouNivel() {
        return PassouNivel;
    }

    public void setPassouNivel(Boolean PassouNivel) {
        this.PassouNivel = PassouNivel;
    }

    public int getArmaAtual() {
        return ArmaAtual;
    }

    public void setArmaAtual(int ArmaAtual) {
        this.ArmaAtual = ArmaAtual;
    }

//    public clsGameNew() {
//    }
    public clsGameNew(clsUsuario usuario, clsArma arma01, clsArma arma02) {
        this.Arma01 = arma01;
        this.Arma02 = arma02;
        this.Usuario = usuario;
        this.ArmaAtual = 1;
        this.Nivel = 1;
        this.PassouNivel = false;
    }

    public clsUsuario getUsuario() {
        return Usuario;
    }

//    public void setUsuario(clsUsuario Usuario) {
//        this.Usuario = Usuario;
//    }

    public clsArma getArma01() {
        return Arma01;
    }

//    public void setArma01(clsArma Arma01) {
//        this.Arma01 = Arma01;
//    }

    public clsArma getArma02() {
        return Arma02;
    }

    //    public void setArma02(clsArma Arma02) {
    //        this.Arma02 = Arma02;
    //    }
    public int getNivel() {
        return Nivel;
    }

    public void setNivel(int Nivel) {
        this.Nivel = Nivel;
    }

    public int getPontosObtidos() {
        return PontosObtidos;
    }

    public void setPontosObtidos(int PontosObtidos) {
        this.PontosObtidos = PontosObtidos;
    }

   

  
}
