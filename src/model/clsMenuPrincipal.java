/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Diego Lacerda
 */
public class clsMenuPrincipal {
    private controller.ControllerPrincipal Controller;

    public clsMenuPrincipal(controller.ControllerPrincipal controller) {
        this.Controller = controller;
    }
    
    
    public clsUsuario logarUsuario(String usuario, String senha){
        
        // Controller.PopularClassUsuario()
        Boolean Existente  = false;
        Boolean SenhaIncorreta = true;
        int indexUsuario = 0;
        List<clsUsuario> tmpListaUsuarios = Controller.PopularClassUsuario();
        clsUsuario UsuarioLogado = new clsUsuario();
        
        for (int i = 0; i < tmpListaUsuarios.size(); i++)
        {
            if (tmpListaUsuarios.get(i).getUsuario().equalsIgnoreCase(usuario)){
                Existente = true;
                
                if (tmpListaUsuarios.get(i).getSenha().equalsIgnoreCase(senha))
                {
                    SenhaIncorreta = false;
                    indexUsuario = i;
                    break;
                }
                else{
                    SenhaIncorreta = true;
                    break;
                }
            }
        }
        

        if (Existente == false)
        {
            JOptionPane.showMessageDialog(null, "Usuario não cadastrado!");
            return null;
        } 
        
        if (SenhaIncorreta == true)
        {
            JOptionPane.showMessageDialog(null, "Senha invalida!");
            return null;
        }
        
        return tmpListaUsuarios.get(indexUsuario);
        
    }
}
