/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Diego Lacerda
 */
public class clsArma {
    private String NomeArma;
    private int Tiros;
    private int Pentes;
    private int TirosTotais;
    private int TirosInicial;

    public clsArma() {
    }

    public String getNomeArma() {
        return NomeArma;
    }

    public void setNomeArma(String NomeArma) {
        this.NomeArma = NomeArma;
    }

    public int getTiros() {
        return Tiros;
    }

    public void setTiros(int Tiros) {
        this.Tiros = Tiros;
        if (TirosInicial <= 0){
            this.TirosInicial = Tiros;    
        }
    }

    public int getPentes() {
        return Pentes;
    }

    public void setPentes(int Pentes) {
        this.Pentes = Pentes;
        //this.TirosTotais = this.Pentes * this.Tiros;
        //Diego Lacerda Alves - 08/11/2013 - Adicionar mais tiros, pois a arma ja vem carregada
        this.TirosTotais = (this.Pentes * this.Tiros) + this.Tiros;
    }

    public int getTirosTotais() {
        return TirosTotais;
    }
    
    public Boolean Disparar( ){
        if (this.TirosTotais == 0){
            return false;
        }
        else
        {

            
            if ((this.Tiros == 1) && (this.Pentes > 0)){
                this.Pentes = this.Pentes - 1;
                this.Tiros = this.TirosInicial;
                
                //this.Tiros = this.Tiros - 1;
                this.TirosTotais = this.TirosTotais - 1;
            }
            else {
                this.Tiros = this.Tiros - 1;
                this.TirosTotais = this.TirosTotais - 1;
            }
                    
            //this.TirosTotais = this.TirosTotais - 1;
            return true;
        } 
    }
    public void Recarregar(){
        
    }
    
}
