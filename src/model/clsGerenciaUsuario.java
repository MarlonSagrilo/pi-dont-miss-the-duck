/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Senac2012
 */
public class clsGerenciaUsuario {

    private ArrayList<clsUsuario> usuarios = (ArrayList<clsUsuario>) PopularClassUsuario();
    
    public static void GravarXmlUsuario(clsUsuario usuarioAdicionado) {
        
        XStream xstream = new XStream();

        File arquivo = new File("Usuario.xml");
        FileOutputStream gravar;
        
        //Carrega a Lista de Usuarios Atual
        ArrayList xmlExistente = (ArrayList<clsUsuario>) xstream.fromXML(arquivo);
        xmlExistente.add(usuarioAdicionado);
        
        String xmlNew;
        xmlNew = xstream.toXML(xmlExistente); 
        
        try {
           FileWriter w = new FileWriter("Usuario.xml");
            w.write(xmlNew);
            w.close();
        } catch (Exception e) {
            System.out.println("Erro ao gravar o XML");
        }
    }
     
   public static List<clsUsuario> PopularClassUsuario(){
        try {
            //Scanner in = new Scanner(new File(NomeXml));
            Scanner in = new Scanner(new File("Usuario.xml"));
            StringBuilder sb = new StringBuilder();
            while (in.hasNext()) {
                //sb.append(in.next()); 
                //espaços da String
                sb.append(in.nextLine()); 
            }
            in.close();
            
            XStream xstream = new XStream();
            //Object[] tmpLista = {0,1};
            
            List<clsUsuario> tmpLista = new ArrayList<clsUsuario>();

            tmpLista =(List<clsUsuario>) xstream.fromXML(sb.toString());

            return tmpLista;
            
        }catch (Exception ex){
            System.out.println("Erro ao ler XML: " + ex);
        }  
        return null;
    }


   // public boolean Alterar(int posicao, String nome, String usuario, String email, String senha) {
//        boolean checked = true;
//        boolean checkEmpty = false;
//        clsUsuario u = usuarios.get(posicao);
//        if (!u.getUsuario().equalsIgnoreCase(usuario)) {
//            for (int i = 0; i < usuarios.size(); i++) {
//                clsUsuario u2 = usuarios.get(i);
//                if (u2.getUsuario().equalsIgnoreCase(usuario)) {
//                    checked = false;
//                }
//            }
//        }
//        if ((checked == true) && (checkEmpty == true)) {
//          GravarXml();
//            //  Adicionar(nome, usuario, senha, email);
//            return true;
//        }
//        return false;

    //}
   


    public void Excluir(int pos) {
        usuarios.remove(pos);

    }
}

