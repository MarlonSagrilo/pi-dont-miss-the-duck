/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import model.clsMenuSecundario;
import model.clsUsuario;

/**
 *
 * @author Diego Lacerda
 */
public class frmMenuSecundario extends javax.swing.JFrame {


    private model.clsMenuSecundario MenuSecundario;
    private controller.ControllerPrincipal Controller;
    public frmMenuSecundario(controller.ControllerPrincipal controller, clsUsuario usuarioLogado) {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.Controller = controller;
        
        MenuSecundario = new clsMenuSecundario(usuarioLogado);
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jButtonIniciarJogo = new javax.swing.JButton();
        jButtonRanking = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(530, 400));
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 204, 255));
        jLabel1.setText("BEM VINDO!!");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(140, 40, 120, 40);

        jButtonIniciarJogo.setBackground(new java.awt.Color(255, 255, 255));
        jButtonIniciarJogo.setForeground(new java.awt.Color(0, 0, 204));
        jButtonIniciarJogo.setText("Iniciar Jogo");
        jButtonIniciarJogo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonIniciarJogoActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonIniciarJogo);
        jButtonIniciarJogo.setBounds(10, 90, 100, 30);

        jButtonRanking.setBackground(new java.awt.Color(255, 255, 255));
        jButtonRanking.setForeground(new java.awt.Color(0, 0, 204));
        jButtonRanking.setText("Ranking");
        jButtonRanking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRankingActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonRanking);
        jButtonRanking.setBounds(10, 160, 100, 30);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Manu 1.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 500, 370);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonIniciarJogoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonIniciarJogoActionPerformed
     Controller.AbrirGame(MenuSecundario.NewUsuario);
    }//GEN-LAST:event_jButtonIniciarJogoActionPerformed

    private void jButtonRankingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRankingActionPerformed
         Controller.AbrirRanking();
    }//GEN-LAST:event_jButtonRankingActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(frmMenuSecundario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(frmMenuSecundario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(frmMenuSecundario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(frmMenuSecundario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new frmMenuSecundario().setVisible(true);
//            }
//        });
//    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonIniciarJogo;
    private javax.swing.JButton jButtonRanking;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
