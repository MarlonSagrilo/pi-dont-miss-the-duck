/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import model.clsGerenciaUsuario;
import view.frmCadUsuario;
import view.frmGameNew;
import view.frmMenuPrincipal;
import view.frmMenuSecundario;
import view.frmRanking;
//import model.clsLogarUsuario;
import model.clsMenuPrincipal;
import model.clsUsuario;

/**
 *
 * @author Diego Lacerda
 */
public class ControllerPrincipal {
    //private frmGameNew JanelaGame;

    //VIEWS
    private frmMenuPrincipal JanelaMenuPrincipal;
    private controller.ControllerMain cMain;
    private frmCadUsuario JanelaCadUsuario;
    private frmMenuSecundario JanelaMenuSecundario;
    private frmRanking JanelaRanking;
    //MODEL
    //private model.clsLogarUsuario mdlLogarUsuario;
    
    private clsUsuario clsUsuario;
    private model.clsGerenciaUsuario MdlGerenciarUsuario;
    private model.clsMenuPrincipal MdlMenuPrincipal;
//    private model.clsGerenciaUsuario alterarUsuario;
//    private model.clsGerenciaUsuario salvarUsuario;

    //private Thread z;
    // private Thread z = new Thread((Runnable) JanelaMenuPrincipal);
    public ControllerPrincipal(ControllerMain clsControllerMain) {
        this.cMain = clsControllerMain;

        //VIEWS
        JanelaMenuPrincipal = new frmMenuPrincipal(this);
        //JanelaMenuPrincipal.setVisible(true);
        JanelaCadUsuario = new frmCadUsuario(this);
        //JanelaMenuSecundario = new frmMenuSecundario(this);
        JanelaRanking = new frmRanking(this);



        JanelaMenuPrincipal.setVisible(true);
        //JanelaCadUsuario.setVisible(true);

        //MODEL
        MdlMenuPrincipal = new clsMenuPrincipal(this);
//        mdlLogarUsuario = new clsLogarUsuario(this);
//        adicionarUsuario = new clsGerenciaUsuario(this);
//        alterarUsuario = new clsGerenciaUsuario(this);
//        salvarUsuario= new clsGerenciaUsuario(this);

        //mdlLogarUsuario = new clsLogarUsuario(this);
        //Thread z = new Thread((Runnable) JanelaMenuPrincipal);
        //z = new ((Thread) frmMenuPrincipal(this);
        // z = new Thread((Runnable) JanelaMenuPrincipal);

        //z = new ;


        //javax.swing.JFrame
        //z.run();

    }

    public void logarUsuario(String usuario, String senha) {
        
        clsUsuario UsuarioLogado = MdlMenuPrincipal.logarUsuario(usuario, senha);
        
        String Tratamento = "";
        try {
           Tratamento = UsuarioLogado.getUsuario();
        } catch (Exception e) {
            
        }
        
        if (Tratamento != ""){
            JOptionPane.showMessageDialog(null, "Usuario " + UsuarioLogado.getUsuario().toString() + " logado com sucesso!"); 
            FecharMenuPrincipal();
            AbrirMenuSecundario(UsuarioLogado);
        }
            
        
  //z.start();
}
    public void FecharMenuPrincipal()
    {
        JanelaMenuPrincipal.setVisible(false);
    }
    public void AbrirCadastroUsuario() {
        JanelaCadUsuario.setVisible(true);

    }
    public void AbrirRanking(){
        JanelaRanking.setVisible(true);
    }
    
    public void AbrirMenuSecundario(clsUsuario usuarioLogado){
        JanelaMenuSecundario = new frmMenuSecundario(this, usuarioLogado);
        JanelaMenuSecundario.setVisible(true);
    }
    
    public void AbrirGame(clsUsuario usuarioLogado){
        cMain.AbrirGame(usuarioLogado);
    }

    public void GravarXmlUsuario(clsUsuario tmpClasse) {
        MdlGerenciarUsuario.GravarXmlUsuario(tmpClasse);
    }

    public List<clsUsuario> PopularClassUsuario() {
        return MdlGerenciarUsuario.PopularClassUsuario();
    }

    public String SalvarUsuario(clsUsuario NewUsuario) {
        if ( (NewUsuario.getNome().isEmpty()) || (NewUsuario.getUsuario().isEmpty())
                || (NewUsuario.getEmail().isEmpty()) || (NewUsuario.getEmail().isEmpty())) {
            return "Campos Obrigatórios";
        }
        
        
        for(int i=0;i<PopularClassUsuario().size();i++){
            clsUsuario u =PopularClassUsuario().get(i);
            if(NewUsuario.getUsuario().equalsIgnoreCase(u.getUsuario())) {
                return "Usuário já consta no sistema";
            }
        }
        
        
        Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
        Matcher m = p.matcher(NewUsuario.getEmail());
        if (!m.find()) {
            return "Email inválido";
        }

        GravarXmlUsuario(NewUsuario);
        return "Usuário Salvo com sucesso!";

    }
}
