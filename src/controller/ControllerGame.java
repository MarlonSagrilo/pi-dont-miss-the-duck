/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import model.Reminder;
import model.clsArma;
import model.clsGameNew;
import model.clsUsuario;
import view.frmGameNew;

/**
 *
 * @author Diego Lacerda
 */
public class ControllerGame {
    private controller.ControllerMain cMain;
    private frmGameNew JanelaGamePrincipal;
    public Timer timer;

    class RemindTask extends TimerTask {
        public void run() {
            
            analisaJogo(JanelaGamePrincipal.GameNow);
            moverPatos();
        }
    }
    void moverPatos(){


        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato1);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato2);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato3);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato4);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato5);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato6);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato7);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato8);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato9);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato10);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato11);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato12);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato13);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato14);
        MoverPatoAleatorio(JanelaGamePrincipal.ImgPato15);

    }
    void analisaJogo(clsGameNew GameNew){
        if (GameNew.getPassouNivel() == true){
            GameNew.setPassouNivel(false);
            GameNew.setPontosObtidos(0);
            JOptionPane.showMessageDialog(null, "Parabéns, você passou de nível!");
            //GameNew.setPassouNivel(false);
            ReiniciarPatos();
        }
    }
    void ReiniciarPatos(){
        JanelaGamePrincipal.ImgPato1.setVisible(true);
        JanelaGamePrincipal.ImgPato2.setVisible(true);
        JanelaGamePrincipal.ImgPato3.setVisible(true);
        JanelaGamePrincipal.ImgPato4.setVisible(true);
        JanelaGamePrincipal.ImgPato5.setVisible(true);
        JanelaGamePrincipal.ImgPato6.setVisible(true);
        JanelaGamePrincipal.ImgPato7.setVisible(true);
        JanelaGamePrincipal.ImgPato8.setVisible(true);
        JanelaGamePrincipal.ImgPato9.setVisible(true);
        JanelaGamePrincipal.ImgPato10.setVisible(true);
        JanelaGamePrincipal.ImgPato11.setVisible(true);
        JanelaGamePrincipal.ImgPato12.setVisible(true);
        JanelaGamePrincipal.ImgPato13.setVisible(true);
        JanelaGamePrincipal.ImgPato14.setVisible(true);
        JanelaGamePrincipal.ImgPato15.setVisible(true);
        
    }
    public void CarregarGame(clsUsuario UsuarioNow){
        //clsUsuario UsuarioNow = new clsUsuario();
        
//        UsuarioNow.setEmail("diego.lacerda.alves@gmail.com");
//        UsuarioNow.setNome("Diego Lacerda Alves");
//        UsuarioNow.setSenha("1234");
//        UsuarioNow.setUsuario("lacerda");
        
        
        clsArma Arma01 = new clsArma();
        Arma01.setNomeArma("MINI-UZI");
        Arma01.setTiros(30);
        Arma01.setPentes(6);
        
        
        
        clsArma Arma02 = new clsArma();
        Arma02.setNomeArma("GLOCK");
        Arma02.setTiros(11);
        Arma02.setPentes(8);
        
        
        
        clsGameNew GameNew = new clsGameNew(UsuarioNow, Arma01, Arma02);
        
        JanelaGamePrincipal = new frmGameNew(this, GameNew);
        JanelaGamePrincipal.setVisible(true);
       
        //IniciarGame();
    }
    public void IniciarGame(){
        timer = new Timer();
        timer.schedule(new RemindTask(), 0, 1 * 1000);  
    }
    
    public ControllerGame(ControllerMain clsControllerMain){
        this.cMain = clsControllerMain;
        
//        clsUsuario UsuarioNow = new clsUsuario();
//        
//        UsuarioNow.setEmail("diego.lacerda.alves@gmail.com");
//        UsuarioNow.setNome("Diego Lacerda Alves");
//        UsuarioNow.setSenha("1234");
//        UsuarioNow.setUsuario("lacerda");
//        
//        
//        clsArma Arma01 = new clsArma();
//        Arma01.setNomeArma("MINI-UZI");
//        Arma01.setTiros(30);
//        Arma01.setPentes(6);
//        
//        
//        
//        clsArma Arma02 = new clsArma();
//        Arma02.setNomeArma("GLOCK");
//        Arma02.setTiros(11);
//        Arma02.setPentes(8);
//        
//        
//        
//        clsGameNew GameNew = new clsGameNew(UsuarioNow, Arma01, Arma02);
//        
//        JanelaGamePrincipal = new frmGameNew(this, GameNew);
//        //JanelaGamePrincipal.setVisible(true);
//       
//        //IniciarGame();
        
    }
    int QtdeMover(clsGameNew GameNow){
        int mover;
        
        if (GameNow.getNivel() ==  1)
        {
            mover = 50;
        }
        else if  (GameNow.getNivel() ==  2){
            mover = 100;
        }
        else if  (GameNow.getNivel() ==  3){
            mover = 150;
        }
        else if  (GameNow.getNivel() ==  4){
            mover = 200;
        }
        else if  (GameNow.getNivel() ==  5){
            mover = 300;
        }
        else{
            mover = 50;
        }
        
        return new Random().nextInt(50) + mover;
    }
    int MoveAleatorio(){
        return new Random().nextInt(4);
    }
    public void MoverPatoAleatorio(javax.swing.JLabel tmpLabel) {
        switch (MoveAleatorio()){
                case 0:
                    MoverPraDireita(tmpLabel);
                    break;
                case 1:
                    MoverPraBaixo(tmpLabel);
                    break;
                case 2:
                    MoverPraEsquerda(tmpLabel);
                    break;
                case 3:
                    MoverPraCima(tmpLabel);
                    break;
        }
       
   }
    public void MoverPraBaixo(javax.swing.JLabel tmpLabel){
         int x,y, tmpWidth, tmpHeight, tmpMover;

        tmpMover = QtdeMover(JanelaGamePrincipal.GameNow);
        x = tmpLabel.bounds().x;
        y = tmpLabel.bounds().y;
        tmpWidth = tmpLabel.bounds().width;
        tmpHeight = tmpLabel.bounds().height;
        
         //if ((tmpMover + y) > JanelaGamePrincipal.bounds().height) 
        if ((tmpMover + y) > JanelaGamePrincipal.bounds().height) 
            {
                //tmpLabel.setBounds(x , tmpMover, tmpWidth, tmpHeight);
                //Diego Lacerda Alves - 08/11/2013 - Para o Pato não sobrepor as armas
                tmpLabel.setBounds(x , tmpMover + 130, tmpWidth, tmpHeight);
            }
        else
            {
                tmpLabel.setBounds(x ,y + tmpMover, tmpWidth, tmpHeight);
            }
         
   } 
    public void MoverPraCima(javax.swing.JLabel tmpLabel){
         int x,y, tmpWidth, tmpHeight,tmpMover;
        
        tmpMover = QtdeMover(JanelaGamePrincipal.GameNow);
        x = tmpLabel.bounds().x;
        y = tmpLabel.bounds().y;
        tmpWidth = tmpLabel.bounds().width;
        tmpHeight = tmpLabel.bounds().height;
        
        //Diego Lacerda Alves - 08/11/2013 - Para o Pato não sobrepor as armas
        //if ((tmpMover + y) <= 130) 
        if ((y - tmpMover) <= 130) 
            
            {
                tmpLabel.setBounds(x ,JanelaGamePrincipal.bounds().height - tmpMover, tmpWidth, tmpHeight);
            }
        else
            {   
                //Diego Lacerda Alves - 08/11/2013 - Para o Pato não sobrepor as armas
//                if ((y - tmpMover) <= 130){
//                    tmpLabel.setBounds(x ,JanelaGamePrincipal.bounds().height - tmpMover, tmpWidth, tmpHeight);
//                }
//                else{
//                    tmpLabel.setBounds(x ,y - tmpMover, tmpWidth, tmpHeight);
//                }
                
               
                    tmpLabel.setBounds(x ,y - tmpMover, tmpWidth, tmpHeight);
               
                
            }
        
   } 
    public void MoverPraDireita(javax.swing.JLabel tmpLabel){
         int x,y, tmpWidth, tmpHeight, tmpMover;
        
        tmpMover = QtdeMover(JanelaGamePrincipal.GameNow);
        x = tmpLabel.bounds().x;
        y = tmpLabel.bounds().y;
        tmpWidth = tmpLabel.bounds().width;
        tmpHeight = tmpLabel.bounds().height;
        
        if ((tmpMover + x) > JanelaGamePrincipal.bounds().width) 
            {
                tmpLabel.setBounds(tmpMover ,y, tmpWidth, tmpHeight);
            }
        else
            {
                tmpLabel.setBounds(x + tmpMover ,y, tmpWidth, tmpHeight);    
            }
            
        
   }
    public void MoverPraEsquerda(javax.swing.JLabel tmpLabel){
         int x,y, tmpWidth, tmpHeight, tmpMover;
        
        tmpMover = QtdeMover(JanelaGamePrincipal.GameNow);
        x = tmpLabel.bounds().x;
        y = tmpLabel.bounds().y;
        tmpWidth = tmpLabel.bounds().width;
        tmpHeight = tmpLabel.bounds().height;
        
        
        if ((tmpMover + x) <= 0) {
            tmpLabel.setBounds(JanelaGamePrincipal.bounds().width - tmpMover ,y, tmpWidth, tmpHeight);
        }
        else
        {
            tmpLabel.setBounds(x - tmpMover ,y, tmpWidth, tmpHeight);    
        }
        
   }
    public Boolean ProcessarClick(clsGameNew GameNew){
        if (GameNew.getArmaAtual() == 1)
            {
                if (GameNew.getArma01().Disparar() == true) {
                    
                    return true;
                }
                else{
                    return false;
                }
          
            }
        else if (GameNew.getArmaAtual() == 2)
            {
                if (GameNew.getArma02().Disparar() == true) {
                    return true;
                }
                else{
                    return false;
                }
            }
        else
            {
                return false;
            }
   } 
    public void AtualizarGame(clsGameNew GameNew){
        
        JanelaGamePrincipal.Tiros01.setText(Integer.toString(GameNew.getArma01().getTiros()));
        JanelaGamePrincipal.Pente01.setText(Integer.toString(GameNew.getArma01().getPentes()));
        JanelaGamePrincipal.Tiros02.setText(Integer.toString(GameNew.getArma02().getTiros()));
        JanelaGamePrincipal.Pente02.setText(Integer.toString(GameNew.getArma02().getPentes()));
        
        JanelaGamePrincipal.Nivel.setText(Integer.toString(GameNew.getNivel()));
        JanelaGamePrincipal.Pontos.setText(Integer.toString(GameNew.getPontosObtidos()));
        
        SelecionarArma(GameNew.getArmaAtual());

    }
    public void SelecionarArma(int arma){
        int left, top;
        this.JanelaGamePrincipal.ArmaSelecionada.setVisible(true);
        if (arma == 1 ){
            left = JanelaGamePrincipal.Arma01.bounds().x - 5;
            top = JanelaGamePrincipal.Arma01.bounds().y - 5;
        }
        else
        {
            left = JanelaGamePrincipal.Arma02.bounds().x - 5;
            top = JanelaGamePrincipal.Arma02.bounds().y - 5;
        }
        
        JanelaGamePrincipal.ArmaSelecionada.setBounds(left ,top, 170, 115);    
        
   }
    public void ProcessarPontos(clsGameNew GameNew){
        
        if ((GameNew.getPontosObtidos() + 1) >= 15) 
        {
            GameNew.setPontosObtidos(GameNew.getPontosObtidos() + 1);
            GameNew.setNivel(GameNew.getNivel() + 1);
            GameNew.setPassouNivel(true);
        }
        else
        {
            GameNew.setPontosObtidos(GameNew.getPontosObtidos() + 1);
        }
        
        
        
    }
}
