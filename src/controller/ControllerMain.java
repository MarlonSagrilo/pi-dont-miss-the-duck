/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.sun.istack.internal.logging.Logger;
import java.util.logging.Level;
import model.clsUsuario;

/**
 *
 * @author Diego Lacerda
 */
public class ControllerMain {
    private controller.ControllerPrincipal c1;
    private controller.ControllerGame c2;
  //  private model.clsLogarUsuario mdlLogarUsuario;
    
    public ControllerMain(){

        this.c1 = new controller.ControllerPrincipal(this);
        //c1.openZ();
        this.c2 = new controller.ControllerGame(this);
        
    }
    
//    public void logarUsuario(String usuario, String senha){
//         //c2.logarUsuario(usuario, senha);
//         //mdlLogarUsuario.logarUsuario(usuario, senha);
//        System.out.println("diego");
//    }
    public void AbrirGame(clsUsuario usuarioLogado){
        c2.CarregarGame(usuarioLogado);
        c2.IniciarGame();
    }
}
