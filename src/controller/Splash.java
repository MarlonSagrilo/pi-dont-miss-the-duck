package controller;


import java.awt.Dimension;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import org.netbeans.lib.awtextra.AbsoluteConstraints;
import org.netbeans.lib.awtextra.AbsoluteLayout;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Diego Lacerda
 */
public class Splash extends JWindow
{
    AbsoluteLayout absoluto;
    AbsoluteConstraints absimage, absbarra;
    ImageIcon image;
    JLabel jLabel;
    JProgressBar barra;
    
    public Splash()
    {
        absoluto = new AbsoluteLayout();
        absimage = new AbsoluteConstraints(0,0);
        //absbarra = new AbsoluteConstraints(0,309);
        absbarra = new AbsoluteConstraints(0,280);
        jLabel = new JLabel();
        //image = new ImageIcon(this.getClass().getResource("ImagemSplash.jpg"));
        image = new ImageIcon(this.getClass().getResource("ImagemSplash.jpg"));
        //image = new ImageIcon(this.getClass().getPackage().
        jLabel.setIcon(image);
        barra = new JProgressBar();
        barra.setPreferredSize(new Dimension(400,10));
        
        this.getContentPane().setLayout(absoluto);
        this.getContentPane().add(jLabel,absimage);
        this.getContentPane().add(barra,absbarra);
        
        new Thread()
        {
            public void run()
            {
                int i = 0;
                while (i<101)
                {
                    barra.setValue(i);
                    i++;
                    
                    //Diego Lacerda Alves - 14/03/2013 - Adicionar aqui o metodo de verificação de BANCO e ARQUIVOS
                    
                    try 
                    {
                    sleep(30);
                    }
                    catch (InterruptedException ex) 
                    {
                        Logger.getLogger(Splash.class.getName()).log(Level.SEVERE,null,ex);
                    }
                }
                
//                frmMdi fMdi = new frmMdi();
//                fMdi.show();
//                fMdi.setVisible(true);
//                fMdi.setLocationRelativeTo(null);
                
                dispose();
            
            }
        }.start();
        
        this.pack();
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }
    
    public static void main(String args[])
    {
        new Splash();
    } 
}
